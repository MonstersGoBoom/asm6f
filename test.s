	; output a C64 prg . 
	; two byte header of load address 
	C64PRG
enum $200 
label	ds24 4 ;	define 4 24bit numbers
label2	ds32 4 ;	define 4 32bit numbers
ende 

	org $8000
	phx 
	phy

	bra reset
	lda #$00 
	sta $d020 
	sta $d021

reset:
	plx 
	ply
	rts

; test int types
int8 $ff 
int16 $1001
int24 $deadf0	
int32 $12345678

